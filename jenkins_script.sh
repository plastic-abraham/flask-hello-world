#!/bin/bash

pip install -r requirements.txt

coverage run --source . -m pytest --junit-xml=unit_test_report.xml

coverage xml

# comment
