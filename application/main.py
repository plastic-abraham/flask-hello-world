"""
API main file
"""

# Stdlib imports

# Flask imports
from flask import Flask


APP = Flask(__name__)
