"""
Starting point
"""

from os import environ
from application.main import APP

if __name__ == '__main__':
    HOST = environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(environ.get('SERVER_PORT', '5557'))
    except ValueError:
        PORT = 5555

    APP.run(host=HOST, port=PORT, debug=True, threaded=False)

